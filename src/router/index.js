import Vue from 'vue'
import Router from 'vue-router'

import Main from '../components/Main/Main'
import Error404 from '../components/Error404'
import Product from '../components/Products/Product'
import ProductDetail from '../components/Products/ProductDetail'
import ContactUs from '../components/Main/ContactUs'
import AboutUs from '../components/Main/AboutUs'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
    },
    {
      path: '*',
      component: Error404,
    },
    {
      path: '/BizeUlasin',
      component: ContactUs,
    },
    {
      path: '/Hakkimizda',
      component: AboutUs,
    },
    {
      path: '/Product',
      component: Product,
		},
    {
      path: '/Product/Detail',
      component: ProductDetail,
    },
  ],
  mode: 'history',
})
